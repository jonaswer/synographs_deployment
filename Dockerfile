FROM python:3.8.15
LABEL author="werheid"

COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
ENTRYPOINT ["python3"]
CMD ["wsgi.py"]
