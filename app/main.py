# Importing flask module in the project is mandatory
# An object of Flask class is our WSGI application.
from flask import Flask, render_template, request
#from Flask import session
#import app.find_synonym_pattern as find_synonym_pattern
import app.find_path as find_path
import app.find_path_eng as find_path_eng
from pyvis.network import Network

app = Flask(__name__, template_folder='templates')

@app.route('/', methods=['GET', 'POST'])
def home():
    return render_template('index_eng.html')

@app.route('/de', methods=['GET', 'POST'])
def home_ger():
    return render_template('index_ger.html')

@app.route('/eng', methods=['GET', 'POST'])
def home_eng():
    return render_template('index_eng.html')

@app.route('/lösung', methods=['GET', 'POST'])
def my_form_post():
    start_word = request.form['start_word']
    end_word = request.form['end_word']

    synonym_path_text = str(find_path.path_API(str(start_word), str(end_word)))
    word_list = str(find_path.wordlist_API(str(start_word), str(end_word)))
    word_list = word_list.replace("[", "").replace("]", "").replace("'", "") + "."
    #nt.show('templates/nx.html')
    return render_template('index2_ger.html', test=synonym_path_text, word_list=word_list)

@app.route('/solution', methods=['POST'])
def my_form_post_eng():
    start_word = request.form['start_word']
    end_word = request.form['end_word']

    synonym_path_text = str(find_path_eng.path_API(str(start_word), str(end_word)))
    word_list = str(find_path_eng.wordlist_API(str(start_word), str(end_word)))
    word_list = word_list.replace("[", "").replace("]", "").replace("'", "")  + "."

    #nt.show('templates/nx.html')
    return render_template('index2_eng.html', test=synonym_path_text, word_list=word_list)

@app.route('/beispiel1')
def examples_ger():
    return render_template('example1_ge.html')

@app.route('/beispiel2')
def examples_ger2():
    return render_template('example2_ge.html')

@app.route('/example1')
def examples_eng():
    return render_template('example1_eng.html')

@app.route('/example2')
def examples_eng2():
    return render_template('example2_eng.html')

