# import libraries
import re
import networkx as nx
import pickle
import sys
import random
import json
# G = nx.Graph()
from pyvis.network import Network


# function to build the nodes of the graph
def set_vertices(G):

    with open('en_thesaurus.jsonl') as f:
        data = [json.loads(line) for line in f]

    for index, line in enumerate(data):
         
        line = line['synonyms']
        line = list(map(lambda x: x.lower(), line))
        for i in range(len(line)):
            re.sub('[^A-Za-z0-9]+', '', line[i])
        line = list(map(lambda x: x.replace(" ", ""), line))
        #print(type(line))
        #print(line)
        line_cleaned = ';'.join(line)
        G.add_node(line_cleaned)

    print("Nodes initialized")
    return


# functions to build the edges of the graph
def set_edges(G):

      for index, data in enumerate(list(G.nodes)):
            print(index/len(list(G.nodes)))
            wordlists_of_pattern = data.split(";")
            for i in range(len(wordlists_of_pattern)):
                for index2, data2 in enumerate(list(G.nodes)):
                    if (index2 != index):
                        wordlist_of_pattern_2_for_comparisson = data2.split(";")
                        for j in range(len(wordlist_of_pattern_2_for_comparisson)):
                            if wordlists_of_pattern[i] == wordlist_of_pattern_2_for_comparisson[j]:
                                G.add_edge(data, data2, weight=wordlists_of_pattern[i])
                            else:
                                continue
                    else:
                        continue

# set methods
def set_graph_as_pickle_obj(G, path):
    nx.write_gpickle(G, path)
    return 0


def set_subgraph_as_pickle_obj(subGraphs):

    object = subGraphs
    file = open('subgraphs2.obj', 'wb')
    pickle.dump(object, file)
    return 0


# get methods
def get_graph_as_pickle_obj(path):
    G = nx.read_gpickle(path)
    return G


def get_subgraph_as_pickle_obj():
    file = open("subgraphs.obj",'rb')
    sub_graphs = pickle.load(file)
    return sub_graphs


def get_strong_subgraphs(G, g, subgraph_list, i):

    initial_node = list(G.nodes)[random.randint(0,len(list(G.nodes)))]
    shortest_path_tree = nx.single_source_shortest_path_length(G, initial_node)

    sub_graphs = g.copy()
    former_cutted_graph = G.copy()

    #get strong subgraph
    for node_as_key, distance_in_graph in shortest_path_tree.items():
        for index, node in enumerate(list(sub_graph.nodes)):
            if node_as_key != node:
                sub_graph.remove_node(node)

    #get cutted graph without strong subgraph
    for node_as_key, distance_in_graph in shortest_path_tree.items():
        for index, node in enumerate(list(former_cutted_graph.nodes)):
            if node_as_key == node:
                former_cutted_graph.remove_node(node)

    return sub_graph


def getShortestPathInGraph(G, start, end):

    first_node = []
    last_node = []

    for index, data in enumerate(list(G.nodes)):
            list_of_words_of_pattern = data.split(";")
            for i in range(len(list_of_words_of_pattern)):
                if list_of_words_of_pattern[i] == start.lower():
                    first_node.append(data)
                if list_of_words_of_pattern[i] == end.lower():
                    last_node.append(data)

    word_list = []

    for i in range(len(first_node)):
        for j in range(len(last_node)):
            try:
                shortest_path = []
                try:
                    shortest_path = nx.shortest_path(G, first_node[i], last_node[j])
                except ValueError:
                    continue
                if len(shortest_path) != 0:
                    break
            except ValueError:
                continue

    if 'shortest_path' in locals():

        for i in range(len(shortest_path)):
            if i == 0:
                word_list.append(start.lower())
            elif i < len(shortest_path)-1 and i != 0:
                for index, word in enumerate(shortest_path[i].split(";")):
                    for index2, word2 in enumerate(shortest_path[i+1].split(";")):
                        if word == word2:
                            word_list.append(word)
        
            elif i == len(shortest_path)-1:
                word_list.append(end.lower())
            else:
                raise ValueError("Something is wrong")
    else: 
        text = "Unfortunately, these words aren't connected by synonyms. Let's try another combination!"
        return text#, G

    text = ""
    text_blocks = [ ["The expression ", " in the context of ", " could also be used as ", " in the context of the word ", ". "],
                    #["You could use the word ", " related to ", " also as ", " in the sense of ", ". "],
                    ["The word ", " with regards to ", " can also be used as ", " in the context of the word ", ". "],
                    #["The expression ", " in the sense of the word ", " could also mean ", " in the sense of ", ". "],
                    ["The word ", " in relation to the meaning of the word ", ", is also used as ", " in the context of of the word ", ". "]
    ]

    G = nx.Graph()

    for i in range(len(word_list)):
        if i<= len(word_list)-2:
            G.add_node(str(word_list[i])+" oder "+str(word_list[i+1]), size=15, title=str(shortest_path[i].split(";")))
        elif i== len(word_list)-1:
            G.add_node(str(word_list[i]), size=15, title=str(shortest_path[i].split(";")))

    for i in range(len(word_list)):
        if i <= len(word_list)-3:
            G.add_edge(str(word_list[i])+" oder "+str(word_list[i+1]),str(word_list[i+1])+" oder "+str(word_list[i+2]))
            #add = " The expression " + str(word_list[i+1]) + " in context of " + str(word_list[i]) + " could also be used as " + str(word_list[i+1]) + " in the context of " + str(word_list[i+2]) + "."
            
            if i == 0:
                index = random.choice(text_blocks) 
                previous_guess = index
                add = index[0] + str(word_list[i+1]) + index[1] + str(word_list[i]) + index[2] + str(word_list[i+1]) + index[3] + str(word_list[i+2]) + index[4] 
                text = text + add
            
            else:
                
                index = random.choice(text_blocks) 
                while index == previous_guess:
                    index = random.choice(text_blocks) 
                previous_guess = index

                add = index[0] + str(word_list[i+1]) + index[1] + str(word_list[i]) + index[2] + str(word_list[i+1]) + index[3] + str(word_list[i+2]) + index[4] 
                text = text + add
            
        elif i == len(word_list)-2:
            G.add_edge(str(word_list[i])+" oder "+str(word_list[i+1]),str(word_list[i+1]))
            #add = " Der Ausdruck " + str(word_list[i+1]).capitalize() + " ist ein Synonym des Ausdruckes " + str(word_list[i]).capitalize()
            #index = random.choice(text_blocks) 
            #add = index[0] + str(word_list[i+1]).capitalize() + index[1] + str(word_list[i]).capitalize() + index[2] + str(word_list[i+1]).capitalize() + index[3] + str(word_list[i+2]).capitalize() + index[4] 
            #text = text + add 
            #print(word_list)

    return text, word_list


def path_API(first_word, last_word):


    G = get_graph_as_pickle_obj("app/graph_eng.pickle")#("app/graph.pickle")
    text, word_list = getShortestPathInGraph(G, first_word, last_word)

    return text

def wordlist_API(first_word, last_word):


    #G = get_graph_as_pickle_obj("graph.pickle")#("app/graph.pickle")
    #text, word_list = getShortestPathInGraph(G, first_word, last_word)
    G = get_graph_as_pickle_obj("app/graph_eng.pickle")#("app/graph.pickle")
    text, word_list = getShortestPathInGraph(G, first_word, last_word)
    #word_list = str(word_list).replace(/[^a-zA-Z0-9 ]/g, '')

    return word_list

if __name__ == "__main__":

    #G = nx.Graph()
    '''initalize graph'''
    #set_vertices(G)
    #set_edges(G)
    #setGraphAsPickleObject(G, "graph.pickle")
    #et_graph_as_pickle_obj(G, "graph.pickle")

    #G = get_graph_as_pickle_obj("graph.pickle")

    #text = path_API("love", "language")
    word_list = wordlist_API("love", "language")
    print(word_list)
    #print(text)
    
    #print(word_list)
    #nx.draw(G, with_labels = True)
    #nt = Network('400px', '400px')
    #nt.from_nx(G)
    #nt.show('templates/pic2.html')
    #'''analyze existence of strong subgraphs'''
    #subGraphList = []
    #i = 1
    #subGraphs = getStrongSubGraphs(G, subGraphList, i)

    #setSubGraphAsPickle(subGraphs)
    #print(len(subGraphList))
    #print(subGraphList)

    #'''Examples:'''
    #text, G = path_API("blatt", "fessel")
    #print(text)
    #nx.draw(G, with_labels = True)
    #nt = Network('600px', '900px')
    #nt.from_nx(G)
    #nt.show('templates/example3.html')
    #getShortestPathInGraph(G, 'blatt', 'fessel')
    #getShortestPathInGraph(G, 'blatt', 'idee')
    #getShortestPathInGraph(G, 'machen', 'tier')
    #getShortestPathInGraph(G, 'stuhl', 'hochzeit')
    #getShortestPathInGraph(G, 'schrank', 'auto')
    #getShortestPathInGraph(G, 'traktor', 'fabrik')
    #text, G = getShortestPathInGraph(G, 'Unsicherheit', 'gewissheit')
    #print(text)
    #components = nx.connected_components(G)
    #components = list(nx.connected_components(G))
    #largest_component = max(components, key=len)

        #print(data[:10])
        
        #line = data[0]
        #print(line)
        #print(line['word'])

      
        
    #break
        

    """
    with open("openthesaurus.txt") as rawdata:
        for index,line in enumerate(rawdata):
            if line.startswith('#'):
                continue
            else:
                dataline_cleaned = re.sub(r"(.*?)\s?\(.*?\)", r"\1", line).lower()
                dataline_cleaned_without_spaces = dataline_cleaned.replace("\n", "")
                dataline_cleaned_without_spaces = dataline_cleaned_without_spaces.replace(" ", "")
                G.add_node(dataline_cleaned_without_spaces)
    print("Nodes initialized")
    return



    """